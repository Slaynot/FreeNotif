# FreeNotif

## Summary

FreeNotif is a python wrapper around the SMS notification API from the french mobile operator Free.

You can use this API after activating the service in your Free dashboard (for no aditionnal costs). It allows you to send SMS to your own phone number (and only that) via a simple GET request.

FreeNotif takes care of the request for you and provides pythonic errors for non documented edge cases (such as message length limit). Therefore it is easy to integrate SMS reports/notifications in any python project.

## Dependencies

None !

## Python version compatibility

FreeNotif was tested with Python 2.7 and 3.6, compatibility with previous versions is not garanteed.

## Usage

1. You first need to activate the service *via* your Free dashboard. You will be provided with your personnal API password.

2. Then, ensure that the device runing your script has an internet connexion configured.

3. You can now start to use FreeNotif. It's as simple as that:

```python
from FreeNotif import FreeNotif

FREE_ID = "12345678"
API_PASSWORD = "81yNpzx0eVPcve"

notif = FreeNotif(FREE_ID, API_PASSWORD)
notif.send_msg("Hello World !")
```

## Return code

The *send_msg* function is returning the HTTP return code replied by the API. Here is the meaning of those codes, as provided by Free:

- 200 : The SMS was sent on your mobile phone.
- 400 : One mandatory parameter is missing.
- 402 : Too much SMS where sent in the time limit.
- 403 : The service is not active for your account, or authentication failure.
- 500 : Server side error. Try again later.

## Exceptions

The *send_msg* function will throw exceptions at you in two cases:

1. Your message is larger than 999 characters, which is the API limit.
2. You are trying to send an empty message, which the API prevents.

## Notes on the API password

It's important to note that when the notification service is enabled, anybody with your API password and Free id can send SMS to your phone number. This means that whoever is able to read your script has all the info required to send you unwanted SMS. Don't forget to apply ACL correctly to limit the risk.

In case of a leak, you can regenerate a new API password by disabling and enabling the service again. This will disable the previous API password and prevent further harm.

## Licence

This work is free. You can redistribute it and/or modify it under the terms of the Do What The Fuck You Want To Public License, Version 2, as published by Sam Hocevar. See the COPYING file for more details.
