# -*- encoding: utf-8 -*-

# Copyright © 2018 Slaynot <slaynot@zaclys.net>
# This work is free. You can redistribute it and/or modify it under the
# terms of the Do What The Fuck You Want To Public License, Version 2,
# as published by Sam Hocevar. See the COPYING file for more details.

import sys

# compatibility tweaks for python 2 and 3
if sys.version_info[0] == 3:
    from urllib.request import urlopen
    from urllib.parse import urlencode
else:
    from urllib import urlopen
    from urllib import urlencode

class FreeNotif(object):

    URL = "https://smsapi.free-mobile.fr/sendmsg"
    MSG_MAX_LENGTH = 999

    def __init__(self, user_id, password):
        """
        @param user_id: your Free user id
        @type user_id: string
        @param password: your API password provided by Free
        @type password: string
        """
        self.user_id = user_id
        self.password = password

    def send_msg(self, message):
        """
        @param message: the message to send
        @type message: string
        @return: the HTTP status code from Free. See doc for details.
        @summary: send the message on the phone. The message must be smaller than 999 chars.
        """
        if len(message) > FreeNotif.MSG_MAX_LENGTH:
            raise Exception("Message max length is {} chars".format(FreeNotif.MSG_MAX_LENGTH))
        if len(message) == 0:
            raise Exception("Message can't be empty")
        print len(message)

        encoded_args = urlencode({"user":self.user_id, "pass":self.password, "msg":message})
        return urlopen(FreeNotif.URL + "?" + encoded_args).getcode()
